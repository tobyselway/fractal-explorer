package main;

import control.Navigation;
import gui.WindowManager;
import io.CursorInput;
import io.ScrollInput;
import org.lwjgl.BufferUtils;
import org.lwjgl.Version;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryStack;
import shaders.BurningShipShader;
import shaders.JuliaShader;
import shaders.MandelbrotShader;
import shaders.ShaderProgram;
import state.Settings;
import state.ViewportState;

import javax.swing.*;
import java.io.File;
import java.net.URL;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Main {

    private ViewportState state = ViewportState.getInstance();

    // The window handle
    private long window;

    public int vWidth = Settings.getInstance().viewportWidth;
    public int vHeight = Settings.getInstance().viewportHeight;

    private CursorInput cursor = new CursorInput(vWidth, vHeight);
    private ScrollInput scroll = new ScrollInput();

    public void run() {
        System.out.println("LWJGL version " + Version.getVersion());

        init();
        loop();

        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    private void init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        // Create the window
        window = glfwCreateWindow(vWidth, vHeight, "Viewport - Fractal Explorer", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // End program when GLFW window is closed
        glfwSetWindowCloseCallback(window, new GLFWWindowCloseCallback() {
            @Override
            public void invoke(long l) {
                System.exit(0);
            }
        });

        glfwSetCursorPosCallback(window, cursor);
        glfwSetScrollCallback(window, scroll);
        glfwSetMouseButtonCallback(window, new GLFWMouseButtonCallback() {
            @Override
            public void invoke(long window, int button, int action, int mods) {
                if(button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                    state.mouseDown = true;
                    state.lastMouseX = cursor.x;
                    state.lastMouseY = cursor.y;
                } else if(button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
                    state.mouseDown = false;
                }
            }
        });

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
        });

        // Automatic resize
        glfwSetWindowSizeCallback(window, new GLFWWindowSizeCallback(){
            @Override
            public void invoke(long window, int width, int height){
                vHeight = height;
                vWidth = width;
            }
        });

        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);

        // Enable v-sync
        if(Settings.getInstance().vSync)
            glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);
    }

    private void loop() {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities();

        // Set the clear color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // Creating an Object

        // Create new VAO
        int vaoID = GL30.glGenVertexArrays();
        // Bind VAO
        GL30.glBindVertexArray(vaoID);
        // Create new VBO
        int vboID = GL15.glGenBuffers();
        // Bind VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
        // Store some data in it
        float[] data = {
                -1f, 1f, 0f,
                -1f, -1f, 0f,
                1f, 1f, 0f,
                1f, -1f, 0f,
                1f, 1f, 0f,
                -1f, -1f, 0f
        };
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        // Store VBO in index 0 in VAO
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);
        GL20.glEnableVertexAttribArray(0);
        // Unbind VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        // Unbind VAO
        GL30.glBindVertexArray(0);

        double currentFrame = glfwGetTime();
        double lastFrame = currentFrame;
        float deltaTime;

        // Instantiate the shader programs
        ShaderProgram mandelbrotShader = new MandelbrotShader();
        ShaderProgram juliaShader = new JuliaShader();
        ShaderProgram burningShipShader = new BurningShipShader();

        ShaderProgram shader = mandelbrotShader;

        // Load palettes
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource("palettes");
        if (url != null) {
            String path = url.getPath();
            File[] listOfFiles = new File(path).listFiles();
            if (listOfFiles != null) {
                for (File listOfFile : listOfFiles) {
                    if (listOfFile.isFile()) {
                        String filename = listOfFile.getName();
                        if(filename.endsWith(".png"))
                            filename = filename.substring(0, filename.length() - 4);
                        int id = shader.loadPalette("palettes/" + filename + ".png");
                        state.palettes.put(filename, id);
                    }
                }
            }
        }

        // Initialize Swing GUI
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        WindowManager wm = WindowManager.getInstance();

        // Launch Swing windows
        wm.init();

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while ( !glfwWindowShouldClose(window) ) {
            currentFrame = glfwGetTime();
            deltaTime = (float) (currentFrame - lastFrame);
            lastFrame = currentFrame;

            glViewport(0, 0, vWidth, vHeight);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

            if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) Navigation.moveStepUp();
            if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) Navigation.moveStepDown();
            if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) Navigation.moveStepLeft();
            if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) Navigation.moveStepRight();

            // Load the correct shader
            switch (state.fractal) {
                case 0:
                    shader = mandelbrotShader;
                    break;
                case 1:
                    shader = juliaShader;
                    break;
                case 2:
                    shader = burningShipShader;
                    break;
            }

            // Start the shader
            shader.start();

            shader.selectPalette(state.palettes.get(state.currentPalette));

            // Update Uniforms
            shader.loadScale(state.scale);
            shader.loadIter(state.iterations);
            shader.loadCx(state.center.x);
            shader.loadCy(state.center.y);
            shader.loadCr(state.c.x);
            shader.loadCi(state.c.y);

            // Rendering the Object
            GL30.glBindVertexArray(vaoID);
            GL15.glDrawArrays(GL15.GL_TRIANGLES, 0, 6);
            GL30.glBindVertexArray(0);

            // Stop the shader
            shader.stop();

            glfwSwapBuffers(window); // swap the color buffers

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();
        }

        // Clean up the shader
        shader.cleanUp();

    }

    public static void main(String[] args) {
        new Main().run();
    }

}

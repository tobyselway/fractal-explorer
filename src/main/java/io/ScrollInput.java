package io;

import control.Navigation;
import org.lwjgl.glfw.GLFWScrollCallback;

public class ScrollInput extends GLFWScrollCallback {

    @Override
    public void invoke(long window, double x, double y) {
        if(y > 0) Navigation.scaleStepUp();
        else Navigation.scaleStepDown();
    }

}

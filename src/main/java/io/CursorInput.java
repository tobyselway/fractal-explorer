package io;

import control.Navigation;
import org.lwjgl.glfw.GLFWCursorPosCallback;

public class CursorInput extends GLFWCursorPosCallback {

    private int windowWidth;
    private int windowHeight;

    public float x = 0;
    public float y = 0;
    public float relX = 0;
    public float relY = 0;

    public CursorInput(int windowWidth, int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
    }

    @Override
    public void invoke(long window, double x, double y) {
        this.x = (float) x;
        this.y = (float) y;
        this.relX = (float) (x / windowWidth * 2 - 1);
        this.relY = (float) (y / windowHeight * 2 - 1) * -1;
        Navigation.moveDrag((float) x, (float) y);
    }

}

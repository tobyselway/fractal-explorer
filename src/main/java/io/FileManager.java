package io;

import state.ViewportState;

import java.io.*;

public class FileManager {

    public static void saveToFile(String filename) {
        try {
            FileOutputStream fout = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(ViewportState.getInstance());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadFromFile(String filename) {
        try {
            FileInputStream fin = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fin);
            ViewportState.getInstance().load((ViewportState) ois.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}

package control;

import state.Settings;
import util.Vector2f;
import state.ViewportState;

public class Navigation {

    private static ViewportState state = ViewportState.getInstance();
    private static Settings settings = Settings.getInstance();

    public static void scale(double factor) {
        state.scale *= factor;
        state.updateAll();
    }

    public static void move(Vector2f d) {
        state.center.x += d.x * state.scale;
        state.center.y += d.y * state.scale;
        state.updateAll();
    }

    public static void moveDrag(float x, float y) {
        if(state.mouseDown) {
            move(new Vector2f(
                    (state.lastMouseX - x) * (-settings.mouseSensitivity),
                    (state.lastMouseY - y) * (settings.mouseSensitivity)
            ));
            state.lastMouseX = x;
            state.lastMouseY = y;
        }
    }

    public static void scaleStepUp() {
        scale(1 - settings.scrollSensitivity);
    }

    public static void scaleStepDown() {
        scale(1 / (1 - settings.scrollSensitivity));
    }

    public static void moveStepUp() {
        move(new Vector2f(0, settings.invertY ? settings.movementSensitivity : -settings.movementSensitivity));
    }

    public static void moveStepDown() {
        move(new Vector2f(0, settings.invertY ? -settings.movementSensitivity : settings.movementSensitivity));
    }

    public static void moveStepLeft() {
        move(new Vector2f(settings.invertX ? -settings.movementSensitivity : settings.movementSensitivity, 0));
    }

    public static void moveStepRight() {
        move(new Vector2f(settings.invertX ? settings.movementSensitivity : -settings.movementSensitivity, 0));
    }

}

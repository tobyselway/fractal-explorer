package gui;

import org.lwjgl.glfw.GLFWVidMode;
import state.Settings;
import state.ViewportState;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionEvent;

import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;

public class NavBox implements Notify {

    private ViewportState state = ViewportState.getInstance();
    private Settings settings = Settings.getInstance();

    private JFrame frame;

    public NavBox() {
        frame = new JFrame("Navigation");
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        frame.setLocation(
                (vidmode.width() / 2) - (settings.viewportWidth / 2) - 7,
                (vidmode.height() / 2) + (settings.viewportHeight / 2) + 15
        );
        frame.setContentPane(mainComponent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();

        update();

        scale.addActionListener(this::onChangeScale);
        iterations.addChangeListener(this::onChangeIterations);
        centerX.addActionListener(this::onChangeCenterX);
        centerY.addActionListener(this::onChangeCenterY);
        cr.addActionListener(this::onChangeCr);
        ci.addActionListener(this::onChangeCi);

        frame.setVisible(true);
    }

    private JPanel mainComponent;
    private JTextField scale;
    private JSpinner iterations;
    private JTextField centerX;
    private JTextField centerY;
    private JTextField cr;
    private JTextField ci;

    public void update() {
        scale.setText(state.scale + "");
        iterations.setValue(state.iterations);
        centerX.setText(state.center.x + "");
        centerY.setText(state.center.y + "");
        cr.setText(state.c.x + "");
        ci.setText(state.c.y + "");
    }

    private void onChangeScale(ActionEvent e) {
        state.scale = Float.parseFloat(scale.getText());
    }

    private void onChangeIterations(ChangeEvent e) {
        state.iterations = (int) iterations.getValue();
    }

    private void onChangeCenterX(ActionEvent e) {
        state.center.x = Float.parseFloat(centerX.getText());
    }

    private void onChangeCenterY(ActionEvent e) {
        state.center.y = Float.parseFloat(centerY.getText());
    }

    private void onChangeCr(ActionEvent e) {
        state.c.x = Float.parseFloat(cr.getText());
    }

    private void onChangeCi(ActionEvent e) {
        state.c.y = Float.parseFloat(ci.getText());
    }
}

package gui;

import state.ViewportState;

public class WindowManager {
    private static WindowManager ourInstance = new WindowManager();

    public static WindowManager getInstance() {
        return ourInstance;
    }

    private ViewportState state = ViewportState.getInstance();

    private Toolbar toolbar;
    private NavBox navBox;
    private SettingsDialog settingsDialog;

    private WindowManager() {
    }

    public void init() {
        toolbar = new Toolbar();
        navBox = new NavBox();
        register();
    }

    public void register() {
        state.registerObjectToNotify(navBox);
    }

    public void openSettings() {
        if(settingsDialog == null) {
            settingsDialog = new SettingsDialog();
        }
        settingsDialog.show();
    }

    public void closeSettings() {
        settingsDialog.hide();
    }

}

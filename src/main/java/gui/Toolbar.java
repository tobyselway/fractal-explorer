package gui;

import io.FileManager;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.util.nfd.NativeFileDialog;
import state.Settings;
import state.ViewportState;

import javax.swing.*;
import java.awt.event.ActionEvent;

import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.util.nfd.NativeFileDialog.NFD_OKAY;

public class Toolbar {

    private ViewportState state = ViewportState.getInstance();
    private Settings settings = Settings.getInstance();
    private WindowManager wm = WindowManager.getInstance();

    private JFrame frame;

    public Toolbar() {
        frame = new JFrame("Toolbar");
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        frame.setLocation(
                (vidmode.width() / 2) - (settings.viewportWidth / 2) - 165,
                (vidmode.height() / 2) - (settings.viewportHeight / 2)
        );
        frame.setContentPane(this.mainComponent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();

        resetButton.addActionListener(this::onReset);
        saveButton.addActionListener(this::onSave);
        loadButton.addActionListener(this::onLoad);
        fractal.addActionListener(this::onChooseFractal);
        settingsButton.addActionListener(this::onSettings);
        palette.addActionListener(this::onChoosePalette);
        aboutButton.addActionListener(this::onAbout);

        loadPalettes();

        frame.setVisible(true);
    }

    private JPanel mainComponent;
    private JButton resetButton;
    private JButton saveButton;
    private JButton loadButton;
    private JComboBox fractal;
    private JButton settingsButton;
    private JButton animateButton;
    private JComboBox<String> palette;
    private JButton aboutButton;

    private void updatePalette() {
        state.currentPalette = (String) this.palette.getSelectedItem();
    }

    private void loadPalettes() {
        for(String p : state.palettes.keySet()) {
            this.palette.addItem(p);
        }
        updatePalette();
    }

    private void onAbout(ActionEvent e) {
        JOptionPane.showMessageDialog(null, "Aplicação desenvolvida por:\nToby Selway (20298)\nProgramação Orientada a Objetos - 2018/19\nInstituto Politécnico de Tomar");
    }

    private void onReset(ActionEvent e) {
        state.reset();
    }

    private void onSave(ActionEvent e) {
        PointerBuffer pb = PointerBuffer.allocateDirect(512);
        int res = NativeFileDialog.NFD_SaveDialog("frac", "", pb);
        if(res == NFD_OKAY) {
            String filename = pb.getStringUTF8();
            if(!filename.endsWith(".frac"))
                filename = filename + ".frac";
            FileManager.saveToFile(filename);
            pb.free();
        }
    }

    private void onLoad(ActionEvent e) {
        PointerBuffer pb = PointerBuffer.allocateDirect(512);
        int res = NativeFileDialog.NFD_OpenDialog("frac", "", pb);
        if(res == NFD_OKAY) {
            FileManager.loadFromFile(pb.getStringUTF8());
            pb.free();
            updateFractal();
        }
    }

    private void updateFractal() {
        fractal.setSelectedIndex(state.fractal);
    }

    private void onChooseFractal(ActionEvent e) {
        state.fractal = fractal.getSelectedIndex();
        state.reset();
    }

    private void onSettings(ActionEvent e) {
        wm.openSettings();
    }

    private void onChoosePalette(ActionEvent e) {
        updatePalette();
    }

    private void createUIComponents() {
        fractal = new JComboBox() {
            // Do not fire if set by program
            protected void fireActionEvent() {
                if(this.hasFocus())
                    super.fireActionEvent();
            }
        };
    }
}

package gui;

import org.lwjgl.glfw.GLFWVidMode;
import state.Settings;

import javax.swing.*;

import java.awt.event.ActionEvent;

import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;

public class SettingsDialog {

    private JFrame frame;

    private Settings settings = Settings.getInstance();

    public SettingsDialog() {
        frame = new JFrame("Settings");
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        frame.setLocation(
                (vidmode.width() / 2),
                (vidmode.height() / 2)
        );
        frame.setContentPane(this.mainComponent);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.pack();

        saveButton.addActionListener(this::onSave);
        cancelButton.addActionListener(this::onCancel);

        show();
    }

    private JSlider mouseSensitivity;
    private JButton cancelButton;
    private JButton saveButton;
    private JCheckBox invertX;
    private JCheckBox invertY;
    private JPanel mainComponent;
    private JSlider keySensitivity;
    private JSlider zoomSensitivity;

    public void show() {
        updateValues();
        frame.setVisible(true);
    }

    public void hide() {
        frame.setVisible(false);
    }

    private void updateValues() {
        mouseSensitivity.setValue((int) (settings.mouseSensitivity * 1000));
        keySensitivity.setValue((int) (settings.movementSensitivity * 1000));
        zoomSensitivity.setValue((int) (settings.scrollSensitivity * 10));
        invertX.setSelected(settings.invertX);
        invertY.setSelected(settings.invertY);
    }

    private void onSave(ActionEvent e) {
        settings.mouseSensitivity = mouseSensitivity.getValue() / 1000f;
        settings.movementSensitivity = keySensitivity.getValue() / 1000f;
        settings.scrollSensitivity = zoomSensitivity.getValue() / 10f;
        settings.invertX = invertX.isSelected();
        settings.invertY = invertY.isSelected();
        hide();
    }

    private void onCancel(ActionEvent e) {
        hide();
    }

}

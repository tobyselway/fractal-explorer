package state;

import gui.Notify;
import util.Vector2f;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ViewportState implements Serializable {
    private transient static ViewportState ourInstance = new ViewportState();

    public static ViewportState getInstance() {
        return ourInstance;
    }

    private transient ArrayList<Notify> objectsToNotify = new ArrayList<>();

    public transient HashMap<String, Integer> palettes = new HashMap<>();
    public transient String currentPalette;

    public transient boolean mouseDown = false;
    public transient float lastMouseX = 0;
    public transient float lastMouseY = 0;

    public int fractal = 0;
    public float scale;
    public int iterations;
    public Vector2f center = new Vector2f(0, 0);
    public Vector2f c = new Vector2f(0, 0);

    private ViewportState() {
        reset();
    }

    public void registerObjectToNotify(Notify obj) {
        objectsToNotify.add(obj);
    }

    public void updateAll() {
        for (Notify obj : objectsToNotify) {
            obj.update();
        }
    }

    public void reset() {
        switch (fractal) {
            case 0: // Mandelbrot defaults
                scale = 1.5f;
                iterations = 30;
                center.x = 0;
                center.y = 0;
                c.x = 0;
                c.y = 0;
                break;
            case 1: // Julia defaults
                scale = 1.5f;
                iterations = 35;
                center.x = 0;
                center.y = 0;
                c.x = 0.45f;
                c.y = 0.1428f;
                break;
            case 2: // BurningShip defaults
                scale = 0.12f;
                iterations = 20;
                center.x = -2.15f;
                center.y = -0.05f;
                c.x = 0;
                c.y = 0;
                break;
        }
        updateAll();
    }

    public void load(ViewportState state) {
        this.fractal = state.fractal;
        this.scale = state.scale;
        this.iterations = state.iterations;
        this.center = state.center;
        this.c = state.c;
        updateAll();
    }

}

package state;

import java.io.Serializable;

public class Settings implements Serializable {
    private static Settings ourInstance = new Settings();

    public static Settings getInstance() {
        return ourInstance;
    }

    public int viewportWidth;
    public int viewportHeight;
    public float scrollSensitivity;
    public float movementSensitivity;
    public float mouseSensitivity;
    public boolean invertX;
    public boolean invertY;
    public boolean vSync;

    private Settings() {
        loadDefault();
    }

    public void loadDefault() {
        viewportWidth = 600;
        viewportHeight = 400;
        scrollSensitivity = 0.2f;
        movementSensitivity = 0.001f;
        mouseSensitivity = 0.005f;
        invertX = false;
        invertY = false;
        vSync = true;
    }

}

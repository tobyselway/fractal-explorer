package util;

import java.io.Serializable;

public class Vector2f implements Serializable {

    public float x;
    public float y;

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }
}

package shaders;

public class BurningShipShader extends ShaderProgram {

    private static final String VERTEX_FILE = "src/main/java/shaders/vertex.glsl";
    private static final String FRAGMENT_FILE = "src/main/java/shaders/fractals/burningship.glsl";

    public BurningShipShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    private int location_scale;
    private int location_iter;
    private int location_cx;
    private int location_cy;

    @Override
    protected void getUniformLocations() {
        this.location_scale = super.getLocation("scale");
        this.location_iter = super.getLocation("iter");
        this.location_cx = super.getLocation("cx");
        this.location_cy = super.getLocation("cy");
    }

    public void loadScale(float scale) {
        super.loadFloat(location_scale, scale);
    }

    public void loadIter(int iter) {
        super.loadInt(location_iter, iter);
    }

    public void loadCx(float cx) {
        super.loadFloat(location_cx, cx);
    }

    public void loadCy(float cy) {
        super.loadFloat(location_cy, cy);
    }

    public void loadCr(float cr) {}

    public void loadCi(float ci) {}

}

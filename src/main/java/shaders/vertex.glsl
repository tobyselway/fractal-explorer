#version 130

in vec3 inPos;
out vec3 pos;

void main() {
    gl_Position = vec4(inPos, 1.0);
    pos = inPos.xyz;
}

package shaders;

import de.matthiasmann.twl.utils.PNGDecoder;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.GL_REPEAT;

public abstract class ShaderProgram {

    private int programID;
    private int vertexShaderID;
    private int fragmentShaderID;

    public ShaderProgram(String vertexFile, String fragmentFile) {
        // Load vertex shader
        vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
        // Load fragment shader
        fragmentShaderID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
        // Create new shader program
        programID = GL20.glCreateProgram();
        // Attach vertex shader to program
        GL20.glAttachShader(programID, vertexShaderID);
        // Attach fragment shader to program
        GL20.glAttachShader(programID, fragmentShaderID);
        // Link program
        GL20.glLinkProgram(programID);
        // Validate program
        GL20.glValidateProgram(programID);
        // Get Uniform Locations
        getUniformLocations();
    }


    public void start() {
        // Use this shader
        GL20.glUseProgram(programID);
    }

    public void stop() {
        // Stop using this shader
        GL20.glUseProgram(0);
    }

    public void cleanUp() {
        // Stop shader
        stop();
        // Detach vertex shader from program
        GL20.glDetachShader(programID, vertexShaderID);
        // Detach fragment shader from program
        GL20.glDetachShader(programID, fragmentShaderID);
        // Delete vertex shader
        GL20.glDeleteShader(vertexShaderID);
        // Delete fragment shader
        GL20.glDeleteShader(fragmentShaderID);
        // Delete shader program
        GL20.glDeleteProgram(programID);
    }


    private static int loadShader(String file, int type) {
        // Read the file and append to shaderSource string
        StringBuilder shaderSource = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null) {
                shaderSource.append(line).append("\n");
            }
            reader.close();
        } catch (IOException e) {
            System.err.println("Could not read file!");
            e.printStackTrace();
            System.exit(-1);
        }

        // Create new shader
        int shaderID = GL20.glCreateShader(type);
        // Put some shader code in it
        GL20.glShaderSource(shaderID, shaderSource);
        // Compile the shader code
        GL20.glCompileShader(shaderID);
        // Check if compiled successfully
        if(GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
            System.err.println("Could not compile shader.");
            System.exit(-1);
        }
        return shaderID;
    }

    protected int getLocation(String uniformName){
        return GL20.glGetUniformLocation(this.programID , uniformName);
    }

    protected abstract void getUniformLocations();

    protected void loadFloat(int location, float value){
        GL20.glUniform1f(location, value);
    }

    protected void loadInt(int location, int value){
        GL20.glUniform1i(location, value);
    }

    public void selectPalette(int p) {
        GL20.glBindTexture(GL20.GL_TEXTURE_1D, p);
    }

    protected int loadTexture1D(ByteBuffer buffer, int width) {
        //create a texture
        int id = GL11.glGenTextures();
        selectPalette(id);
        GL20.glTexParameteri(GL20.GL_TEXTURE_1D, GL20.GL_TEXTURE_MIN_FILTER, GL20.GL_NEAREST);
        GL20.glTexParameteri(GL20.GL_TEXTURE_1D, GL20.GL_TEXTURE_MAG_FILTER, GL20.GL_NEAREST);
        GL20.glTexParameteri(GL20.GL_TEXTURE_1D, GL20.GL_TEXTURE_WRAP_S, GL_REPEAT);

        GL20.glTexImage1D(GL20.GL_TEXTURE_1D, 0, 4, width, 0, GL20.GL_RGBA, GL15.GL_UNSIGNED_BYTE, buffer);

        GL20.glEnable(GL20.GL_TEXTURE_1D);
        return id;
    }

    public int loadPalette(String filename) {
        try {
            //load png file
            PNGDecoder decoder = new PNGDecoder(getClass().getClassLoader().getResourceAsStream(filename));
            //create a byte buffer big enough to store RGBA values
            ByteBuffer buffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());

            //decode
            decoder.decode(buffer, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);

            //flip the buffer so its ready to read
            buffer.flip();
            return this.loadTexture1D(buffer, decoder.getWidth());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public abstract void loadScale(float scale);
    public abstract void loadIter(int iter);
    public abstract void loadCx(float cx);
    public abstract void loadCy(float cy);
    public abstract void loadCr(float cr);
    public abstract void loadCi(float ci);

}

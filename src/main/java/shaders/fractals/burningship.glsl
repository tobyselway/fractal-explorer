#version 130

out vec4 FragColor;
in vec3 pos;

uniform sampler1D palette;

uniform float scale = 1;
uniform int iter = 50;
uniform float cx = 0;
uniform float cy = 0;
uniform float cr = 0;
uniform float ci = 0;

void main() {

    vec2 z, c;
    vec2 center = vec2(cx, cy);

    c.x = 1.3333 * (pos.x) * scale - center.x - 0.5;
    c.y = (pos.y) * scale - center.y;

    int i;
    z = c;
    for(i = 0; i < iter; i++) {
        float x = (abs(z.x) * abs(z.x) - abs(z.y) * abs(z.y)) - c.x;
        float y = (abs(z.y) * abs(z.x) + abs(z.x) * abs(z.y)) - c.y;

        if((x * x + y * y) > 4.0) break;
        z.x = x;
        z.y = y;
    }

    gl_FragColor = texture1D(palette, (i == iter ? 0.0 : float(i)) / 100.0);

    /*
        float r = 0;
        float g = 100;
        float b = 255;

        gl_FragColor = (i == iter ? vec4(0, 0, 0, 1) : vec4(float(i) / iter * r/255, float(i) / iter * g/255, float(i) / iter * b/255, 1));
    */

}
